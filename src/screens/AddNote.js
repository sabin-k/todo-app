import {
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React, {useState} from 'react';
import firestore from '@react-native-firebase/firestore';
// import AsyncStorage from '@react-native-async-storage/async-storage';

import {COLORS, SIZES} from '../constants/Theme';
import AddLine from '../assets/svgs/add-line.svg';
import Loading from '../components/reusable/Loading';
// import {createUUID} from '../utilities/helperFunction';

const AddNote = ({navigation, route}) => {
  // const {notes} = route.params;

  const [title, setTitle] = useState('');
  const [subtitle, setSubTitle] = useState('');
  const [tag, setTag] = useState('');
  const [tags, setTags] = useState([]);
  const [data, setData] = useState('');
  const [loading, setLoading] = useState(false);

  const fireStore = firestore().collection('Notes');

  // const storeData = async value => {
  //   try {
  //     const jsonValue = JSON.stringify(value);
  //     await AsyncStorage.setItem('notes', jsonValue);
  //     navigation.goBack();
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  const addTag = () => {
    let tagsHolder = [];

    if (tags) {
      tagsHolder = [...tags, tag];
    } else {
      tagsHolder = [tag];
    }

    setTags(tagsHolder);
    setTag('');
  };

  const storeDataInFirebase = async note => {
    setLoading(true);
    try {
      fireStore
        .add(note)
        .then(response => {
          console.log('Note Added', response);
          setLoading(false);
          navigation.goBack();
        })
        .catch(error => {
          setLoading(false);
          console.log('Note Not Added', error);
        });
    } catch (e) {
      setLoading(false);
      console.log('Error', e);
    }
  };

  const addToDo = () => {
    const newItem = {
      // id: createUUID(),
      title: title,
      subtitle: subtitle,
      data: data,
      type: 'note',
      createdBy: 'Sabin Khanal',
      tags: tags,
      isCompleted: false,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };

    // let newObject = null;

    // if (notes) {
    //   newObject = [...notes, newItem];
    // } else {
    //   newObject = [newItem];
    // }

    storeDataInFirebase(newItem);
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Create Note</Text>

      <TextInput
        placeholder="Title"
        placeholderTextColor={COLORS.gray}
        style={styles.titleInputField}
        value={title}
        blurOnSubmit
        onChangeText={val => {
          setTitle(val);
        }}
        numberOfLines={2}
      />

      <TextInput
        placeholder="Subtitle"
        placeholderTextColor={COLORS.gray}
        style={styles.subtitleInputField}
        value={subtitle}
        blurOnSubmit
        onChangeText={val => {
          setSubTitle(val);
        }}
        numberOfLines={2}
      />

      {tags.length > 0 && (
        <View
          style={{
            width: SIZES.width * 0.9,
            alignSelf: 'center',
            marginTop: SIZES.padding * 2,
            flexDirection: 'row',
          }}>
          <Text
            style={{
              color: COLORS.primary,
              fontFamily: 'Lato-Bold',
              fontSize: SIZES.h3,
              textAlign: 'left',
              marginRight: SIZES.padding,
            }}>
            Tags
          </Text>

          <View style={styles.tagsContainer}>
            {tags.length > 0 && (
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}>
                {tags.map((tag, index) => (
                  <Text key={index.toString()} style={styles.tagsText}>
                    {tag}
                  </Text>
                ))}
              </ScrollView>
            )}
          </View>
        </View>
      )}

      <View
        style={{
          flexDirection: 'row',
          marginTop: SIZES.padding * 2,
          alignSelf: 'center',
          alignItems: 'flex-start',
          justifyContent: 'space-between',
          width: SIZES.width * 0.9,
        }}>
        <TextInput
          placeholder="Add tag"
          placeholderTextColor={COLORS.gray}
          autoCapitalize={false}
          style={styles.tagInputField}
          value={tag}
          blurOnSubmit
          onChangeText={val => {
            setTag(val);
          }}
          numberOfLines={1}
        />
        <Pressable
          style={styles.tagAddButton}
          onPress={() => {
            addTag();
          }}>
          <AddLine width={24} height={24} fill={COLORS.white} />
        </Pressable>
      </View>

      <TextInput
        placeholder="Write something"
        placeholderTextColor={COLORS.gray}
        textAlignVertical={'top'}
        style={styles.inputField}
        value={data}
        blurOnSubmit
        onChangeText={val => {
          setData(val);
        }}
        multiline={true}
      />

      <View style={styles.buttonWrapper}>
        <Pressable style={styles.submitButton} onPress={() => addToDo()}>
          <Text
            style={{
              color: COLORS.white,
              fontFamily: 'Lato-Regular',
              fontSize: SIZES.h4,
            }}>
            Submit
          </Text>
        </Pressable>

        <Pressable
          style={styles.cancelButton}
          onPress={() => navigation.goBack()}>
          <Text
            style={{
              color: COLORS.primary,
              fontFamily: 'Lato-Regular',
              fontSize: SIZES.h4,
            }}>
            Cancel
          </Text>
        </Pressable>
      </View>
    </View>
  );
};

export default AddNote;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 35 : 0,
  },
  titleText: {
    marginTop: SIZES.padding * 2,
    color: COLORS.primary,
    fontFamily: 'Lato-Black',
    fontSize: SIZES.h2,
    textAlign: 'left',
    alignSelf: 'center',
    width: SIZES.width * 0.9,
  },
  subtitleInputField: {
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h4,
    marginTop: SIZES.padding * 2,
    alignSelf: 'center',
    width: SIZES.width * 0.9,
    height: 50,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    padding: SIZES.padding,
    color: COLORS.primary,
  },
  titleInputField: {
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h3,
    marginTop: SIZES.padding * 2,
    alignSelf: 'center',
    width: SIZES.width * 0.9,
    height: 50,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    padding: SIZES.padding,
    color: COLORS.primary,
  },
  tagInputField: {
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h4,
    width: SIZES.width * 0.75,
    height: 50,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    padding: SIZES.padding,
    color: COLORS.primary,
  },
  tagAddButton: {
    width: 50,
    height: 50,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h4,
    marginTop: SIZES.padding * 2,
    alignSelf: 'center',
    width: SIZES.width * 0.9,
    height: SIZES.height * 0.54,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    padding: SIZES.padding,
    color: COLORS.primary,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: SIZES.width * 0.9,
    alignSelf: 'center',
    marginTop: 20,
  },
  submitButton: {
    height: 50,
    width: SIZES.width * 0.4,
    alignSelf: 'center',
    backgroundColor: COLORS.primary,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelButton: {
    height: 50,
    width: SIZES.width * 0.4,
    alignSelf: 'center',
    backgroundColor: COLORS.white,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tagsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '87%',
    flex: 1,
  },
  tagsText: {
    backgroundColor: COLORS.secondary,
    borderRadius: SIZES.radius * 0.5,
    color: COLORS.gray,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h5,
    textTransform: 'capitalize',
    marginRight: SIZES.padding * 0.5,
    paddingVertical: SIZES.padding * 0.5,
    paddingHorizontal: SIZES.padding * 0.6,
  },
});
