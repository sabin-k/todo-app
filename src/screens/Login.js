import React from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

import {COLORS, SIZES} from '../constants/Theme';

const Login = () => {
  GoogleSignin.configure({
    webClientId:
      '933128520166-426fv6j8dk6lknsgeb07icsdivppatfp.apps.googleusercontent.com',
  });

  async function handleGoogleButtonPress() {
    try {
      // get the user id token
      const {idToken} = await GoogleSignin.signIn();
      // create a credential using the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      console.log('googleCredential', googleCredential);
      console.log('idToken', idToken);
      // authenticate the user using the credential
      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      console.log('error', error);
    }
  }

  return (
    <View style={styles.screen}>
      <Pressable onPress={() => handleGoogleButtonPress()}>
        <Text
          style={{
            backgroundColor: COLORS.primary,
            color: COLORS.white,
            elevation: SIZES.elevation,
            padding: SIZES.padding * 1.5,
            borderRadius: SIZES.radius,
          }}>
          Sign In With Google
        </Text>
      </Pressable>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  screen: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    marginBottom: 30,
    color: COLORS.primary,
  },
});
