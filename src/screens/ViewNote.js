import {Platform, StyleSheet, Text, View} from 'react-native';
import React from 'react';

import {COLORS, SIZES} from '../constants/Theme';
import Header from '../components/Header';
import {getDate} from '../utilities/helperFunction';

const ViewNote = ({route}) => {
  const {note} = route.params;

  return (
    <View style={styles.container}>
      <Header />

      <View
        style={{
          paddingVertical: SIZES.padding * 2,
          paddingHorizontal: SIZES.padding * 2,
        }}>
        <Text style={styles.titleText}>{note.title}</Text>

        <View style={styles.descriptionWrapper}>
          <Text style={styles.descriptionLabel}>Created by</Text>
          <Text style={styles.descriptionText}>{note.createdBy}</Text>
        </View>

        {note.createdAt != note.updatedAt && (
          <View style={styles.descriptionWrapper}>
            <Text style={styles.descriptionLabel}>Modified At</Text>
            <Text style={styles.descriptionText}>
              {getDate(note.updatedAt, 'dd MMMM yyyy, h:mm a')}
            </Text>
          </View>
        )}

        <View style={styles.descriptionWrapper}>
          <Text style={styles.descriptionLabel}>Created At</Text>
          <Text style={styles.descriptionText}>
            {getDate(note.createdAt, 'dd MMMM yyyy, h:mm a')}
          </Text>
        </View>

        {note.tags?.length > 0 && (
          <View style={styles.descriptionWrapper}>
            <Text style={styles.descriptionLabel}>Tags</Text>
            {note.tags?.length > 0 && (
              <View style={styles.tagsWrapper}>
                {note.tags.map((tag, index) => (
                  <Text key={index.toString()} style={styles.tagsText}>
                    {tag}
                  </Text>
                ))}
              </View>
            )}
          </View>
        )}
      </View>

      <View style={styles.contentSeperator} />

      <View
        style={{
          paddingVertical: SIZES.padding * 2,
          paddingHorizontal: SIZES.padding * 2,
        }}>
        {note.subtitle && (
          <Text style={styles.subtitleText}>{note.subtitle}</Text>
        )}

        <Text style={styles.bodyText}>{note.data}</Text>
      </View>
    </View>
  );
};

export default ViewNote;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
    marginTop: Platform.OS === 'ios' ? 35 : 0,
  },
  titleText: {
    color: COLORS.primary,
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h1,
  },
  descriptionLabel: {
    color: COLORS.gray,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h5,
    marginRight: SIZES.padding * 2,
  },
  descriptionText: {
    color: COLORS.primary,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h5,
  },
  descriptionWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.padding * 2,
  },
  tagsWrapper: {
    flexDirection: 'row',
    width: '80%',
    overflow: 'scroll',
    alignItems: 'center',
  },
  tagsText: {
    backgroundColor: COLORS.secondary,
    borderRadius: SIZES.radius * 0.5,
    color: COLORS.gray,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h5,
    textTransform: 'capitalize',
    marginRight: SIZES.padding * 0.5,
    paddingVertical: SIZES.padding * 0.5,
    paddingHorizontal: SIZES.padding * 0.6,
  },
  subtitleText: {
    color: COLORS.primary,
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h3,
    marginBottom: SIZES.padding * 2,
  },
  bodyText: {
    color: COLORS.primary,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h3,
  },
  contentSeperator: {
    height: 1,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.secondary,
    marginTop: SIZES.padding,
  },
});
