import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, StyleSheet, Platform, Pressable} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
// import AsyncStorage from '@react-native-async-storage/async-storage';

import {COLORS, SIZES} from '../constants/Theme';
import Loading from '../components/reusable/Loading';
import Header from '../components/Header';
import AddLine from '../assets/images/add-line.svg';
import ListNotes from '../components/ListNotes';
// import {createUUID} from '../utilities/helperFunction';

const HomeScreen = () => {
  const navigation = useNavigation();
  const fireStore = firestore().collection('Notes');

  const [loading, setLoading] = useState(false);
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    getFirebaseData();
  }, []);

  useFocusEffect(
    useCallback(() => {
      // your logic goes here
      // getData();
    }, []),
  );

  // useEffect(() => {
  //   storeData(todos);
  // }, []);

  // const storeData = async value => {
  //   // setLoading(true);
  //   try {
  //     const jsonValue = JSON.stringify(value);
  //     await AsyncStorage.setItem('todos', jsonValue);
  //     // setLoading(false);
  //     getData();
  //   } catch (e) {
  //     console.log(e);
  //     // setLoading(false);
  //   }
  // };

  const getFirebaseData = () => {
    setLoading(true);

    try {
      fireStore.onSnapshot(querySnapshot => {
        const list = [];
        querySnapshot?.forEach(doc => {
          const {
            title,
            data,
            updatedAt,
            createdAt,
            createdBy,
            tags,
            type,
            subtitle,
          } = doc.data();

          list.push({
            id: doc.id,
            title,
            data,
            updatedAt,
            createdAt,
            createdBy,
            tags,
            type,
            subtitle,
          });
        });

        setNotes(list);
      });

      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };

  // const getData = async () => {
  //   setLoading(true);
  //   try {
  //     const jsonValue = await AsyncStorage.getItem('notes');
  //     const data = jsonValue != null ? JSON.parse(jsonValue) : [];
  //     setNotes(data);
  //     setLoading(false);
  //   } catch (e) {
  //     console.log(e);
  //     setLoading(false);
  //   }
  // };

  // const deleteTodo = id => {
  //   const newList = notes.filter(item => item.id !== id);
  //   storeData(newList);
  //   // getData();
  // };

  // const completedTodo = id => {
  //   notes.find(todo => todo.id === id).isCompleted = true;
  //   storeData(notes);
  // };

  // const incompleteTodo = id => {
  //   notes.find(todo => todo.id === id).isCompleted = false;
  //   storeData(notes);
  // };

  // const updateTodo = (id, value) => {
  //   notes.find(todo => todo.id === id).task = value;
  //   storeData(notes);
  // };

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.mainContainer}>
      <Header />

      <View style={{flex: 1}}>
        <ListNotes notes={notes} />

        <View style={styles.floatingButtonWrapper}>
          <Pressable
            android_ripple={COLORS.white}
            onPress={() =>
              navigation.navigate('AddNote', {
                notes: notes,
              })
            }
            style={styles.addNewButton}>
            <AddLine width={24} height={24} fill={COLORS.white} />
            <Text style={{color: COLORS.white, fontFamily: 'Lato-Regular'}}>
              Add New Note
            </Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
    marginTop: Platform.OS === 'ios' ? 35 : 0,
  },
  floatingButtonWrapper: {
    position: 'absolute',
    height: SIZES.height * 0.15,
    width: SIZES.width,
    bottom: 0,
    backgroundColor: 'rgba(255,255,255,0.4)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addNewButton: {
    height: 50,
    width: SIZES.width * 0.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: COLORS.primary,
    borderRadius: SIZES.radius,
    flexDirection: 'row',
    elevation: SIZES.elevation,
  },
});
