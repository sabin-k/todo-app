import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import auth from '@react-native-firebase/auth';

import HomeScreen from '../screens/HomeScreen';
import AddNote from '../screens/AddNote';
import ViewNote from '../screens/ViewNote';
import Login from '../screens/Login';

const App = createNativeStackNavigator();
const Auth = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <App.Screen name="Home" component={HomeScreen} />
      <App.Screen name="AddNote" component={AddNote} />
      <App.Screen name="ViewNote" component={ViewNote} />
    </App.Navigator>
  );
};

const AuthNavigator = () => {
  return (
    <Auth.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Auth.Screen name="Login" component={Login} />
    </Auth.Navigator>
  );
};

const RootNavigator = () => {
  const [authenticated, setAuthenticated] = useState(false);

  auth().onAuthStateChanged(user => {
    if (user) {
      setAuthenticated(true);
    } else {
      setAuthenticated(false);
    }
  });

  return (
    <NavigationContainer>
      {authenticated ? <AppNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

export default RootNavigator;
