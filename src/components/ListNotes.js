import React from 'react';
import {View, StyleSheet, Text, FlatList, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {COLORS, SIZES} from '../constants/Theme';
import {getDate} from '../utilities/helperFunction';

const ListNotes = ({notes}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={notes}
        contentContainerStyle={{
          paddingBottom: 140,
          paddingHorizontal: SIZES.padding,
          paddingTop: SIZES.padding,
        }}
        renderItem={({item}) => (
          <View style={styles.itemWrapper}>
            <Pressable
              style={styles.gridCards}
              onPress={() =>
                navigation.navigate('ViewNote', {
                  note: item,
                })
              }>
              <Text style={styles.dateText}>
                {getDate(item.updatedAt, 'dd MMM')}
              </Text>

              {item.tags?.length > 0 && (
                <View style={styles.tagsWrapper}>
                  {item.tags.slice(0, 3).map((tag, index) => (
                    <Text key={index.toString()} style={styles.tagsText}>
                      {tag}
                    </Text>
                  ))}

                  {item.tags.length > 3 && (
                    <Text style={styles.tagsText}>+{item.tags.length - 3}</Text>
                  )}
                </View>
              )}
              <Text style={styles.titleText}>{item.title}</Text>
              <Text style={styles.bodyText}>{item.data}</Text>
            </Pressable>
          </View>
        )}
        numColumns={2}
        keyExtractor={item => item.id}
      />
    </View>
  );
};
export default ListNotes;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
    width: SIZES.width,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: SIZES.padding,
    marginTop: SIZES.padding * 2,
  },
  gridCards: {
    padding: SIZES.padding,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    minHeight: 125,
    backgroundColor: COLORS.secondary,
    borderRadius: SIZES.radius,
  },
  titleText: {
    marginTop: SIZES.padding * 0.7,
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h4,
    color: COLORS.primary,
  },
  dateText: {
    fontFamily: 'Lato-Bold',
    fontSize: SIZES.h6,
    color: COLORS.gray,
    letterSpacing: 0.5,
    textTransform: 'uppercase',
  },
  tagsWrapper: {
    flexDirection: 'row',
    marginTop: SIZES.padding * 0.7,
    width: '100%',
    overflow: 'hidden',
  },
  tagsText: {
    backgroundColor: COLORS.secondaryDark,
    borderRadius: SIZES.radius * 0.25,
    color: COLORS.gray,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h7,
    textTransform: 'capitalize',
    marginRight: SIZES.padding * 0.5,
    paddingVertical: SIZES.padding * 0.1,
    paddingHorizontal: SIZES.padding * 0.5,
  },
  bodyText: {
    marginTop: SIZES.padding * 0.7,
    fontFamily: 'Lato-Regular',
    fontSize: SIZES.h5,
    color: COLORS.primary,
  },
});
