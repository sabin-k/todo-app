import {StyleSheet, Text, View, Pressable} from 'react-native';
import React from 'react';
import {useRoute, useNavigation} from '@react-navigation/native';

import {COLORS, SIZES} from '../constants/Theme';
import Search from '../assets/images/search.svg';
import More from '../assets/images/more.svg';
import ArrowLeft from '../assets/svgs/arrow-left.svg';

const Header = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const routes = navigation.getState()?.routes;
  const prevRoute = routes[routes.length - 2];

  console.log('prevRoute', prevRoute);

  return (
    <View style={styles.headerContainer}>
      {route.name == 'Home' && (
        <View style={{flex: 1}}>
          <Text style={styles.headerText}>Sabin</Text>
        </View>
      )}

      {route.name == 'ViewNote' && (
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Pressable
            onPress={() => navigation.goBack()}
            style={{
              borderRadius: SIZES.radius,
              padding: SIZES.padding / 2,
              marginRight: SIZES.padding,
            }}>
            <ArrowLeft width={24} height={24} fill={COLORS.primary} />
          </Pressable>

          <Text style={styles.headerText}>{prevRoute.name}</Text>
        </View>
      )}

      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <Pressable onPress={() => {}} style={styles.iconButton}>
          <Search width={24} height={24} fill={COLORS.primary} />
        </Pressable>

        <Pressable onPress={() => {}} style={styles.iconButton}>
          <More width={24} height={24} fill={COLORS.primary} />
        </Pressable>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: COLORS.white,
    height: 70,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.secondary,
    flexDirection: 'row',
    paddingHorizontal: SIZES.padding * 2,
  },
  headerText: {
    fontFamily: 'Lato-Black',
    color: COLORS.primary,
    fontSize: SIZES.h2,
  },
  iconButton: {
    borderRadius: SIZES.radius,
    padding: SIZES.padding / 2,
    marginLeft: SIZES.padding,
  },
});
