import React, {useState} from 'react';
import {StyleSheet, View, Pressable, TextInput} from 'react-native';

import {COLORS, SIZES} from '../constants/Theme';
import Plus from '../assets/images/plus-square-black.svg';

const AddTodo = ({addToDo}) => {
  const [item, setItem] = useState('');

  const _addTodo = () => {
    if (item && item.length > 0) {
      addToDo(item);
      setItem('');
    }
  };

  return (
    <View style={styles.addContainer}>
      <TextInput
        style={styles.textInput}
        placeholder={`Create a new task`}
        value={item}
        onChangeText={val => {
          setItem(val);
        }}
        placeholderTextColor={COLORS.white}
        // returnKeyType={'done'}
        autoCorrect={false}
        // onSubmitEditing={() => {
        //   addToDo(item);
        //   setItem('');
        // }}
        multiline={true}
      />

      <Pressable onPress={() => _addTodo()} style={styles.addToDoButton}>
        <Plus width={24} height={24} />
      </Pressable>
    </View>
  );
};

export default AddTodo;

const styles = StyleSheet.create({
  addContainer: {
    alignSelf: 'center',
    width: SIZES.width * 0.9,
    marginTop: SIZES.padding * 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
  },
  textInput: {
    height: '100%',
    width: '78%',
    textAlign: 'left',
    padding: SIZES.padding,
    backgroundColor: COLORS.primary,
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: SIZES.radius,
    fontSize: SIZES.h5,
    elevation: SIZES.elevation,
    color: COLORS.white,
  },
  addToDoButton: {
    backgroundColor: COLORS.primary,
    borderRadius: SIZES.radius,
    fontSize: SIZES.h5,
    width: '18%',
    elevation: SIZES.elevation,
    color: COLORS.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
