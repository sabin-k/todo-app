import React from 'react';
import {Text, View, ActivityIndicator, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants/Theme';

const Loading = () => {
  return (
    <View style={styles.loadingContainer}>
      <Text style={{color: COLORS.primary, marginBottom: SIZES.padding * 2}}>
        Syncing...
      </Text>
      <ActivityIndicator size={'large'} color={COLORS.primary} />
    </View>
  );
};

export default Loading;

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
