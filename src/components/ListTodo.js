import React, {useState} from 'react';
import {
  Pressable,
  StyleSheet,
  Text,
  View,
  TextInput,
  Platform,
} from 'react-native';

import {COLORS, SIZES} from '../constants/Theme';

import Trash from '../assets/images/trash-white.svg';
import Edit from '../assets/images/edit-white.svg';
import Check from '../assets/images/check-white.svg';

const ListTodo = ({
  todo,
  deleteItem,
  completedTask,
  incompleteTask,
  updateTask,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [toDoValue, setToDoValue] = useState('');

  const toggleTaskStatus = () => {
    if (todo.isCompleted) {
      incompleteTask(todo.id);
    } else {
      completedTask(todo.id);
    }
  };

  const startEditing = () => {
    setToDoValue(todo.task);
    setIsEditing(true);
  };

  const finishEditing = () => {
    updateTask(todo.id, toDoValue);
    setIsEditing(false);
  };

  const controlInput = value => {
    setToDoValue(value);
  };

  return (
    <View style={styles.listContainer}>
      <View style={styles.rowContainer}>
        <Pressable onPress={toggleTaskStatus}>
          <View
            style={[
              styles.radioButton,
              todo.isCompleted ? styles.radioActive : styles.radioInActive,
            ]}
          />
        </Pressable>

        {isEditing ? (
          <TextInput
            value={toDoValue}
            style={[
              styles.editInputStyle,
              styles.textStyle,
              todo.isCompleted ? styles.completeText : styles.incompleteText,
            ]}
            multiline={true}
            returnKeyType={'done'}
            onBlur={finishEditing}
            onChangeText={controlInput}
          />
        ) : (
          <Text
            style={[
              styles.textStyle,
              todo.isCompleted ? styles.completeText : styles.incompleteText,
            ]}>
            {todo.task}
          </Text>
        )}
      </View>

      {isEditing ? (
        <View style={styles.buttonContainer}>
          <Pressable onPressOut={finishEditing}>
            <View style={styles.buttonStyle}>
              <Check width={18} height={18} stroke={COLORS.white} />
            </View>
          </Pressable>
        </View>
      ) : (
        <View style={styles.buttonContainer}>
          <Pressable onPressOut={startEditing}>
            <View style={styles.buttonStyle}>
              <Edit width={18} height={18} stroke={COLORS.white} />
            </View>
          </Pressable>
          <Pressable
            onPressOut={() => {
              deleteItem(todo.id);
            }}>
            <View style={styles.buttonStyle}>
              <Trash width={18} height={18} stroke={COLORS.white} />
            </View>
          </Pressable>
        </View>
      )}
    </View>
  );
};

export default ListTodo;

const styles = StyleSheet.create({
  listContainer: {
    width: SIZES.width * 0.9,
    borderBottomColor: COLORS.primary,
    borderBottomWidth: SIZES.hairline,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: SIZES.padding / 2,
    height: 'auto',
    paddingVertical: SIZES.padding / 2,
  },
  textStyle: {
    fontSize: SIZES.h5,
    color: COLORS.black,
  },
  radioButton: {
    width: 18,
    height: 18,
    borderRadius: SIZES.radiusx1,
    borderWidth: 1,
    marginRight: SIZES.padding,
  },
  radioActive: {
    borderColor: COLORS.primary,
  },
  radioInActive: {
    borderColor: COLORS.gray,
  },
  completeText: {
    color: COLORS.gray,
    textDecorationLine: 'line-through',
  },
  incompleteText: {
    color: COLORS.black,
  },
  rowContainer: {
    flexDirection: 'row',
    width: Platform.OS === 'ios' ? SIZES.width / 2.2 : SIZES.width / 1.8,
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  buttonStyle: {
    marginHorizontal: 5,
    backgroundColor: COLORS.offwhite,
    height: 24,
    width: 24,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.radius / 2,
  },
  editInputStyle: {
    width: SIZES.width / 1.8,
    paddingBottom: 5,
  },
});
