import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export const COLORS = {
  //base colors
  primary: '#0B0A0A',
  secondary: '#E9E9E9',
  secondaryDark: '#D6D6D6',

  //colors
  white: '#FFFFFF',
  black: '#000000',
  gray: '#676767',
  blue: '#3498DB',
  green: '#2ECC71',
  red: '#E74C3C',
  offwhite: '#F2F2F2',
};

export const SIZES = {
  //global sizes
  hairline: 1,
  tiny: 3,
  smaller: 8,
  small: 12,
  base: 16,
  large: 20,
  larger: 24,
  radius: 10,
  radiusx1: 100,
  padding: 10,
  elevation: 3,

  //font sizes
  largeTitle: 50,
  h1: 30,
  h2: 24,
  h3: 20,
  h4: 18,
  h5: 16,
  h6: 14,
  h7: 12,

  //device dimensions
  width,
  height,
};

const Theme = {COLORS, SIZES};

export default Theme;
